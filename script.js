let oneNumber;
let oneCharacterBlock = false;
let presentCharacter;
let blockPoint = false;
let resetFlag = false;
let lastScore = 0;

function loadButtons()
{
	for(let i = 9; i>0; i--)
	{
		let myButton = document.createElement("button");
		let v = document.createTextNode(i);
		myButton.appendChild(v);
		
		myButton.id = 'bt'+i;
		document.getElementById("buttons").appendChild(myButton);
		
		myButton.onclick = function() {writeOne(i)};
		
	}
}

function writeOne(character)
{
	if(resetFlag==true) {deleteAll(); resetFlag = false}
	oneNumber = Number(character);	//zawiera cyfry od 0-9 lub NaN w przypadku wciśnięcia innych znaków
	console.log(character);				
	
	if(oneNumber || character==0 || !oneCharacterBlock)
	{
		if(character != '.' || !blockPoint) 
		{
		if(document.getElementById("wynik").value != '' || oneNumber || character==0)	//zabezpiecza przed rozpoczęcia działania znakiem
			document.getElementById("wynik").value += character;	//zabezpieczenie przed wciśnięciem dwóch kropek w jednej liczbie ale tylko po lewej stronie :( 
		}
	}
	
	if(!oneNumber && character!=0 && !oneCharacterBlock) 	//jesli wcisnęliśmy znak inny od cyfry 0-9 ii jesli aktualnie nie mamy zablokowanej możliwości wpiwywania kolejnych znaków
	{
		if(character == '.') {blockPoint=Boolean(true);}
		else 
		{
			oneCharacterBlock = Boolean(true);	//blokuj możliwość wpisywania kolejnych znaków (mnożenie dzielenie etc.)
			presentCharacter=character;				//zawiera ostatnio użyty znak 
		}				
	}
	console.log(blockPoint);
}
function deleteOne()
{
	let valueToEdit = document.getElementById("wynik").value;
	
	document.getElementById("wynik").value = valueToEdit.substring(0, valueToEdit.length-1);
}

function count()
{
	
	 document.form.wynik.value = eval(document.form.wynik.value);
    let wynik = document.form.wynik.value;
    localStorage.setItem("wynik",wynik);
	lastScore = wynik;
	oneCharacterBlock = false;
	blockPoint = false;
	presentCharacter = null;
	resetFlag = true;
    
}
function deleteAll()
{
	document.getElementById("lastScore").value = "Poprzedni wynik: " + lastScore;
	oneCharacterBlock = false;
	blockPoint = false;
	presentCharacter = null;
	document.form.wynik.value = '';
	
}
